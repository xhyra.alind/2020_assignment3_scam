package it.rage_against_the_svm.scam.models;

import javax.persistence.*;

@Entity
public class App extends Service {

    private Integer devicesNumber;

    private String compatibility;

    public Integer getDevicesNumber() {
        return devicesNumber;
    }

    public void setDevicesNumber(Integer devicesNumber) {
        this.devicesNumber = devicesNumber;
    }

    public String getCompatibility() {
        return compatibility;
    }

    public void setCompatibility(String compatibility) {
        this.compatibility = compatibility;
    }
}
