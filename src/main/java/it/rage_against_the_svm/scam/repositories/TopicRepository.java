package it.rage_against_the_svm.scam.repositories;

import it.rage_against_the_svm.scam.models.Topic;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TopicRepository extends CrudRepository<Topic, Integer> {
    @Query("select distinct b from Topic a join a.include as b " +
            "where b.name like %?1% " +
            "and b.description like %?2% " +
            "and a.id in ?3")
    List<Topic> findTopics(String name, String description, List<Integer> parents);

    @Query("select distinct a from Topic a " +
            "where a.name like %?1% " +
            "and a.description like %?2% ")
    List<Topic> findTopics(String name, String description);
}
