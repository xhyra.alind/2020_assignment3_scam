package it.rage_against_the_svm.scam.repositories;

import it.rage_against_the_svm.scam.models.App;
import it.rage_against_the_svm.scam.models.Service;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ServiceRepository extends CrudRepository<Service, Integer> {
    @Query("select distinct a from Service a join a.label t " +
            "where a.link like %?1% " +
            "and a.userAvg >= ?2 " +
            "and a.name like %?3% " +
            "and a.hosting like %?4% " +
            "and a.admin.id in ?5 " +
            "and t.id in ?6 ")
    List<Service> findServices(String link,
                               double userAvg,
                               String name,
                               String hosting,
                               List<Integer> adminList,
                               List<Integer> topicList);

    @Query("select distinct a from Service a " +
            "where a.link like %?1% " +
            "and a.userAvg >= ?2 " +
            "and a.name like %?3% " +
            "and a.hosting like %?4% " +
            "and a.admin.id in ?5 ")
    List<Service> findServices(String link,
                               double userAvg,
                               String name,
                               String hosting,
                               List<Integer> adminList);

    @Query("select distinct a from App a join a.label t " +
            "where a.link like %?1%" +
            " and a.userAvg >= ?2" +
            " and a.name like %?3%" +
            " and a.hosting like %?4% " +
            "and a.admin.id in ?5 " +
            "and t.id in ?6 " +
            "and a.devicesNumber >= ?7 " +
            "and a.compatibility like %?8%")
    List<App> findApps(String link,
                       double userAvg,
                       String name,
                       String hosting,
                       List<Integer> adminList,
                       List<Integer> topicList,
                       Integer devicesNumber,
                       String compatibility);

    @Query("select distinct a from App a " +
            "where a.link like %?1%" +
            " and a.userAvg >= ?2" +
            " and a.name like %?3%" +
            " and a.hosting like %?4% " +
            "and a.admin.id in ?5 " +
            "and a.devicesNumber >= ?6 " +
            "and a.compatibility like %?7%")
    List<App> findApps(String link,
                       double userAvg,
                       String name,
                       String hosting,
                       List<Integer> adminList,
                       Integer devicesNumber,
                       String compatibility);
}

