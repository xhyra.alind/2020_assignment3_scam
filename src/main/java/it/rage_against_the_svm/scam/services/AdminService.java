package it.rage_against_the_svm.scam.services;

import it.rage_against_the_svm.scam.exceptions.AdminNotFoundException;
import it.rage_against_the_svm.scam.models.Admin;
import it.rage_against_the_svm.scam.repositories.AdminRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AdminService implements ScamService<Admin> {

    private final AdminRepository adminRepository;

    public AdminService(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    public void create(Admin admin) {
        adminRepository.save(admin);
    }

    public void update(int id, Admin admin) {
        Optional<Admin> a = adminRepository.findById(id);
        if (a.isPresent()) {
            Admin newAdmin = a.get();
            newAdmin.setName(admin.getName());
            newAdmin.setEmail(admin.getEmail());
            newAdmin.setVatNumber(admin.getVatNumber());
            adminRepository.save(newAdmin);
        } else {
            throw new AdminNotFoundException();
        }
    }

    public void delete(int id) {
        if (adminRepository.findById(id).isPresent())
            adminRepository.deleteById(id);
        else
            throw new AdminNotFoundException();
    }

    @Override
    public boolean isPresent(int id) {
        return adminRepository.existsById(id);
    }

    public Admin getOne(int id) {
        Optional<Admin> a = adminRepository.findById(id);
        if (a.isPresent())
            return a.get();
        else
            throw new AdminNotFoundException();
    }

    public List<Admin> getAdmins(String name, String mail, String vatnumber) {
        return adminRepository.findAdmins(name, mail, vatnumber);
    }

    public List<Admin> getAll() {
        return (List<Admin>) adminRepository.findAll();
    }

}
