package it.rage_against_the_svm.scam.services;

import it.rage_against_the_svm.scam.exceptions.AppNotFoundException;
import it.rage_against_the_svm.scam.exceptions.ServiceNotFoundException;
import it.rage_against_the_svm.scam.models.App;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AppService implements ScamService<App> {

    private final ServiceService serviceService;

    public AppService(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    public void create(Map<String, Object> app) {
        serviceService.create(app, true);
    }

    public void create(App app) {
        serviceService.create(app);
    }

    public void update(int id, App app) {
        try {
            serviceService.update(id, app);
        } catch (ServiceNotFoundException e) {
            throw new AppNotFoundException(e);
        }
    }

    public void update(Map<String, Object> app) {
        serviceService.update(app, true);
    }

    public void delete(int id) {
        try {
            serviceService.delete(id);
        } catch (ServiceNotFoundException e) {
            throw new AppNotFoundException(e);
        }
    }

    public boolean isPresent(int id) {
        return serviceService.isPresent(id);
    }

    public App getOne(int id) {
        try {
            return (App) serviceService.getOne(id);
        } catch (ServiceNotFoundException e) {
            throw new AppNotFoundException(e);
        }
    }

    public List<App> getApps(String link,
                             double userAvg,
                             String name,
                             String hosting,
                             List<Integer> adminList,
                             List<Integer> topicsList,
                             Integer devicesNumber,
                             String compatibility) {

        return serviceService.getApps(link,
                                      userAvg,
                                      name,
                                      hosting,
                                      adminList,
                                      topicsList,
                                      devicesNumber,
                                      compatibility);
    }

    @Override
    public List<it.rage_against_the_svm.scam.models.Service> getAll() {
        List<it.rage_against_the_svm.scam.models.Service> services = serviceService.getAll();
        ArrayList<it.rage_against_the_svm.scam.models.Service> apps = new ArrayList<>();

        services.forEach(s -> {
            if (s instanceof App) apps.add(s);
        });

        return apps;
    }

}
