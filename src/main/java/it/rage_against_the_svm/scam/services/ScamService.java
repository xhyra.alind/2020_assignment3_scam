package it.rage_against_the_svm.scam.services;

public interface ScamService<T> {
    Object getOne(int id);
    Object getAll();
    void create(T element);
    void update(int id, T object);
    void delete(int id);
    boolean isPresent(int id);
}
