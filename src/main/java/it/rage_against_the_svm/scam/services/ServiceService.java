package it.rage_against_the_svm.scam.services;

import it.rage_against_the_svm.scam.exceptions.ServiceNotFoundException;
import it.rage_against_the_svm.scam.models.App;
import it.rage_against_the_svm.scam.models.Topic;
import it.rage_against_the_svm.scam.repositories.ServiceRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ServiceService implements ScamService<it.rage_against_the_svm.scam.models.Service> {

    final
    AdminService adminService;
    final
    TopicService topicService;

    private final ServiceRepository serviceRepository;

    public ServiceService(ServiceRepository serviceRepository, AdminService adminService, TopicService topicService) {
        this.serviceRepository = serviceRepository;
        this.adminService = adminService;
        this.topicService = topicService;
    }

    private void createOrUpdate(Map<String, Object> service, boolean isApp) {
        it.rage_against_the_svm.scam.models.Service newService;
        int id = Integer.MIN_VALUE;
        int numberDevice = Integer.MIN_VALUE;
        String link, name, hosting;
        String compatibility = null;
        double userAvg;

        link = (String) service.get("link");
        userAvg = Double.parseDouble((String) service.get("userAvg"));
        name = (String) service.get("name");
        hosting = (String) service.get("hosting");
        if (isApp) {
            numberDevice = Integer.parseInt((String) service.get("numberDevice"));
            compatibility = (String) service.get("compatibility");
        }

        if (service.containsKey("id")) {
            id = Integer.parseInt((String) service.get("id"));
            newService = this.getOne(id);
        } else {
            if (isApp) {
                newService = new App();
            } else {
                newService = new it.rage_against_the_svm.scam.models.Service();
            }
        }

        newService.setName(name);
        newService.setLink(link);
        newService.setHosting(hosting);
        newService.setUserAvg(userAvg);

        if (isApp) {
            ((App) newService).setDevicesNumber(numberDevice);
            ((App) newService).setCompatibility(compatibility);
        }

        newService.setAdmin(adminService.getOne(Integer.parseInt((String)service.get("admin"))));

        @SuppressWarnings("unchecked")
        List<String> topics = (ArrayList<String>) service.get("topics");

        Topic topic;
        List<Topic> TopicsList = new ArrayList<>();

        for (String s : topics) {
            topic = topicService.getOne(Integer.parseInt(s));
            TopicsList.add(topic);
        }
        newService.setLabel(TopicsList);

        if (service.containsKey("id"))
            this.update(id, newService);
        else
            this.create(newService);
    }

    public void create(it.rage_against_the_svm.scam.models.Service service) {
        serviceRepository.save(service);
    }

    public void create(Map<String, Object> service) {
        this.create(service, false);
    }

    public void create(Map<String, Object> service, boolean isApp) {
        this.createOrUpdate(service, isApp);
    }

    public void update(int id, it.rage_against_the_svm.scam.models.Service service) {
        Optional<it.rage_against_the_svm.scam.models.Service> s = serviceRepository.findById(id);
        if (s.isPresent()) {
            it.rage_against_the_svm.scam.models.Service newService = s.get();
            newService.setName(service.getName());
            newService.setHosting(service.getHosting());
            newService.setLink(service.getLink());
            newService.setUserAvg(service.getUserAvg());
            newService.setLabel(service.getLabel());
            newService.setAdmin(service.getAdmin());
            if (service instanceof App) {
                ((App) newService).setCompatibility(((App) service).getCompatibility());
                ((App) newService).setDevicesNumber(((App) service).getDevicesNumber());
            }
            serviceRepository.save(newService);
        } else
            throw new ServiceNotFoundException();
    }

    public void update(Map<String, Object> service) {
        this.update(service, false);
    }

    public void update(Map<String, Object> service, boolean isApp) {
        this.createOrUpdate(service, isApp);
    }



    public void delete(int id) {
        if (serviceRepository.existsById(id))
            serviceRepository.deleteById(id);
        else
            throw new ServiceNotFoundException();
    }

    public boolean isPresent(int id) {
        return serviceRepository.existsById(id);
    }

    public it.rage_against_the_svm.scam.models.Service getOne(int id) {
        Optional<it.rage_against_the_svm.scam.models.Service> s = serviceRepository.findById(id);
        if (s.isPresent())
            return s.get();
        else
            throw new ServiceNotFoundException();
    }

    public List<it.rage_against_the_svm.scam.models.Service> getServices(String link,
                                                                         double userAvg,
                                                                         String name,
                                                                         String hosting,
                                                                         List<Integer> adminList,
                                                                         List<Integer> topicList){
        if (topicList.isEmpty()) {
            return serviceRepository.findServices(link, userAvg, name, hosting, adminList);
        }
        else {
            return serviceRepository.findServices(link, userAvg, name, hosting, adminList, topicList);
        }
    }

    public List<it.rage_against_the_svm.scam.models.App> getApps(String link,
                                                                 double userAvg,
                                                                 String name,
                                                                 String hosting,
                                                                 List<Integer> adminList,
                                                                 List<Integer> topicList,
                                                                 Integer devicesNumber,
                                                                 String compatibility) {
        if (topicList.isEmpty()) {
            return serviceRepository.findApps(link,
                                              userAvg,
                                              name,
                                              hosting,
                                              adminList,
                                              devicesNumber,
                                              compatibility);
        }
        else {
            return serviceRepository.findApps(link,
                                              userAvg,
                                              name,
                                              hosting,
                                              adminList,
                                              topicList,
                                              devicesNumber,
                                              compatibility);
        }
    }

    public List<it.rage_against_the_svm.scam.models.Service> getAll() {
        return (List<it.rage_against_the_svm.scam.models.Service>) serviceRepository.findAll();
    }

}
