package it.rage_against_the_svm.scam.services;

import com.google.gson.Gson;
import it.rage_against_the_svm.scam.exceptions.TopicNotFoundException;
import it.rage_against_the_svm.scam.models.Topic;
import it.rage_against_the_svm.scam.repositories.TopicRepository;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TopicService implements ScamService<Topic> {

    private final TopicRepository topicRepository;
    private HashMap<Integer, ArrayList<Integer>> parents;
    private List<Integer> visited;

    public TopicService(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
        this.parents = new HashMap<>();
    }

    private void createOrUpdateTopic(Map<String, Object> topic) {
        Topic newTopic;

        String name = (String) topic.get("name");
        String description = (String) topic.get("description");
        @SuppressWarnings("unchecked")
        List<String> parents = (ArrayList<String>) topic.get("parents");

        if (topic.containsKey("id"))
            newTopic = this.getOne(Integer.parseInt((String) topic.get("id")));
        else
            newTopic = new Topic();

        newTopic.setName(name);
        newTopic.setDescription(description);
        Topic parent = null;
        List<Topic> children;

        if (topic.containsKey("id")) {
            if (this.parents.containsKey(newTopic.getId())) {
                for (Integer p : this.parents.get(newTopic.getId())) {
                    List<Topic> listTmp = this.getOne(p).getInclude();
                    listTmp.remove(newTopic);
                }
            }
        }

        for (String s : parents) {
            parent = this.getOne(Integer.parseInt(s));
            if (parent.getInclude() == null) {
                children = new ArrayList<>();
            } else {
                children = parent.getInclude();
            }
            children.add(newTopic);
            parent.setInclude(children);
        }

        if (topic.containsKey("id")) {
            this.update(Integer.parseInt((String) topic.get("id")), newTopic);
        } else {
            this.create(newTopic);
        }

        if (parent != null)
            this.update(parent.getId(), parent);
        updateParents();

    }

    public void create(Topic topic) {
        topicRepository.save(topic);
    }

    public void create(Map<String, Object> topic) {
        createOrUpdateTopic(topic);
    }

    public void update(int id, Topic topic) {
        Optional<Topic> t = topicRepository.findById(id);
        if (t.isPresent()) {
            Topic newTopic = t.get();
            newTopic.setName(topic.getName());
            newTopic.setLabel(topic.getLabel());
            newTopic.setDescription(topic.getDescription());
            topicRepository.save(newTopic);
        } else
            throw new TopicNotFoundException();
    }

    public void update(Map<String, Object> topic) {
        createOrUpdateTopic(topic);
    }

    private void recDelete(Topic t) {
        if (t.getInclude() == null) {
            topicRepository.deleteById(t.getId());
        } else {
            for (Topic i : t.getInclude()) {
                if (visited.contains(i.getId())) {
                    return;
                } else if (parents.get(i.getId()).size() == 1) {
                    visited.add(i.getId());
                    recDelete(i);
                    topicRepository.deleteById(i.getId());
                }
            }
        }
    }

    public void delete(int id) {
        if (topicRepository.findById(id).isPresent()) {
            Topic t = this.getOne(id);

            visited = new ArrayList<>();

            visited.add(t.getId());
            recDelete(t);
            topicRepository.deleteById(id);
        } else
            throw new TopicNotFoundException();
    }

    public boolean isPresent(int id) {
        return topicRepository.existsById(id);
    }

    public Topic getOne(int id) {
        Optional<Topic> t = topicRepository.findById(id);
        if (t.isPresent()) {
            return t.get();
        }
        throw new TopicNotFoundException();
    }

    public List<Topic> getAll() {
        return (List<Topic>) topicRepository.findAll();
    }

    public Map<Integer, String> getNames() {
        ArrayList<Topic> topics = (ArrayList<Topic>) this.getAll();
        HashMap<Integer, String> names = new HashMap<>();
        for (Topic i: topics) {
            names.put(i.getId(), i.getName());
        }
        return names;
    }

    public void updateParents() {
        ArrayList<Topic> topics = (ArrayList<Topic>) topicRepository.findAll();
        parents = new HashMap<>();
        for (Topic i : topics) parents.computeIfAbsent(i.getId(), k -> new ArrayList<>());
        for (Topic i : topics) {
            if (i.getInclude() == null)
                continue;
            for (Topic j : i.getInclude()) {
                ArrayList<Integer> p;
                if (parents.containsKey(j.getId())) {
                    p = parents.get(j.getId());
                } else {
                    p = new ArrayList<>();
                }
                p.add(i.getId());
                parents.put(j.getId(), p);
            }
        }

    }


    public List<Topic> getTopics(String name, String description, List<Integer> parents){
        if(parents.isEmpty()){
            return topicRepository.findTopics(name, description);
        }else {
            return topicRepository.findTopics(name, description, parents);
        }
    }

    public HashMap<Integer, ArrayList<Integer>> getParents() {
        updateParents();
        return parents;
    }

    public String getGraph() {
        ArrayList<Topic> topics = (ArrayList<Topic>) topicRepository.findAll();

        ArrayList<String> nodes_raw = new ArrayList<>();
        for (Topic topic : topics) {
            try {
                String temp = new JSONObject()
                        .put("id", topic.getId())
                        .put("label", topic.getName())
                        .toString();
                nodes_raw.add(temp);
            } catch (Exception ignored) {
            }
        }

        String nodes = nodes_raw.toString();

        ArrayList<String> edges = new ArrayList<>();

        for (Topic i: topics){
            if (i.getInclude() == null) {
                continue;
            }
            for (Topic j: i.getInclude()){
                try {
                    String value = new JSONObject()
                            .put("from", i.getId())
                            .put("to", j.getId())
                            .toString();
                    edges.add(value);
                } catch (Exception ignored) {
                }
            }
        }

        HashMap<Integer, String> desc = new HashMap<>();
        HashMap<Integer, ArrayList<Integer>> edges2 = new HashMap<>();
        for (Topic i: topics) {
            ArrayList<Integer> adjacency = new ArrayList<>();
            i.getInclude().forEach((T) -> adjacency.add(T.getId()));

            desc.put(i.getId(), i.getDescription());
            edges2.put(i.getId(), adjacency);
        }

        return "{\"nodes\":" + nodes +
                ",\n\"edges\": " + edges.toString() + "," +
                "\n\"description\":" + new Gson().toJson(desc) + ","+
                "\n\"adjacencies\":" + new Gson().toJson(edges2) + "," +
                "\n\"parents\":" + new Gson().toJson(parents) +
                "}\n";
    }

}
