package it.rage_against_the_svm.scam.controllers;

import it.rage_against_the_svm.scam.models.Admin;
import it.rage_against_the_svm.scam.models.App;
import it.rage_against_the_svm.scam.models.Service;
import it.rage_against_the_svm.scam.models.Topic;
import it.rage_against_the_svm.scam.services.AdminService;
import it.rage_against_the_svm.scam.services.AppService;
import it.rage_against_the_svm.scam.services.ServiceService;
import it.rage_against_the_svm.scam.services.TopicService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ServiceController {

    public ServiceController(ServiceService serviceService,
                             AdminService adminService,
                             TopicService topicService,
                             AppService appService) {
        this.serviceService = serviceService;
        this.adminService = adminService;
        this.topicService = topicService;
        this.appService = appService;
    }

    @RequestMapping("/service")
    public String servicePanel() {return "service";}

    private final ServiceService serviceService;
    private final AdminService adminService;
    private final TopicService topicService;
    private final AppService appService;

    @GetMapping("/service/admin/{id}")
    public String serviceByAdmin(@PathVariable Integer id, Model m) {

        List<Service> allServices = serviceService.getAll();
        List<Admin> admins = adminService.getAll();
        List<App> apps = new ArrayList<>();
        List<Service> services = new ArrayList<>();

        HashMap<Integer, String> names= new HashMap<>();
        HashMap<Integer, String> topicNames= new HashMap<>();

        admins.forEach(a -> names.put(a.getId(), a.getName()));

        allServices.forEach(s -> {
            if (s.getAdmin().getId().equals(id)) {
                if (s instanceof App)
                    apps.add((App)s);
                else
                    services.add(s);
            }
        });
        
        m.addAttribute("services", services);
        m.addAttribute("admins", names);
        m.addAttribute("topics", topicNames);
        m.addAttribute("apps", apps);

        return "service";
    }

    @GetMapping("/service")
    public String servicePanel(Model m,
                               @RequestParam(name="link",
                                             required = false,
                                             defaultValue = "") String link,
                               @RequestParam(name="userAvg",
                                             required = false,
                                             defaultValue = "0") String userAvg,
                               @RequestParam(name="name",
                                             required = false,
                                             defaultValue = "") String name,
                               @RequestParam(name="hosting",
                                             required = false,
                                             defaultValue = "") String hosting,
                               @RequestParam(name= "adminIds",
                                             required = false) List<String> adminIds,
                               @RequestParam(name= "topicIds",
                                             required = false) List<String> topicIds,
                               @RequestParam(name= "type",
                                             required = false,
                                             defaultValue = "Service") String type,
                               @RequestParam(name="devicesNumber",
                                             required = false,
                                             defaultValue = "0") String devicesNumber,
                               @RequestParam(name="compatibility",
                                             required = false,
                                             defaultValue = "") String compatibility) {

        List<Integer> topicRequestedIds = new ArrayList<>();
        List<Integer> adminRequestedIds = new ArrayList<>();

        if (topicIds != null) {
            topicIds.forEach(a -> topicRequestedIds.add(Integer.parseInt(a)));
        }
        if (adminIds != null && !adminIds.isEmpty()) {
            adminIds.forEach(a -> adminRequestedIds.add(Integer.parseInt(a)));
        } else {
            if (adminService.getAll().isEmpty())
                return "error_service";
            adminService.getAll().forEach(a -> adminRequestedIds.add((a.getId())));
        }

        List<Service> services = new ArrayList<>();

        if(type.equals("Service")) {
            services = serviceService.getServices(link,
                                                  Double.parseDouble(userAvg),
                                                  name,
                                                  hosting,
                                                  adminRequestedIds,
                                                  topicRequestedIds);
        }

        List<Admin> admins = adminService.getAll();
        List<Topic> topics = topicService.getAll();
        List<App> apps = appService.getApps(link,
                                            Double.parseDouble(userAvg),
                                            name,
                                            hosting,
                                            adminRequestedIds,
                                            topicRequestedIds,
                                            Integer.parseInt(devicesNumber),
                                            compatibility);

        services.removeAll(apps);

        HashMap<Integer, String> adminNames= new HashMap<>();
        HashMap<Integer, String> topicNames= new HashMap<>();

        admins.forEach(a -> adminNames.put(a.getId(), a.getName()));

        for (Topic i: topics) {
            topicNames.put(i.getId(), i.getName());
        }

        m.addAttribute("services", services);
        m.addAttribute("admins", adminNames);
        m.addAttribute("topics", topicNames);
        m.addAttribute("apps", apps);

        return "service";
    }

    @PostMapping("/service")
    public ResponseEntity<String> addService(@RequestBody Map<String, Object> service) {
        boolean isApp = (boolean) service.get("checkbox");
        if (isApp) {
            appService.create(service);
        } else {
            serviceService.create(service);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/service/{id}")
    public ResponseEntity<String> deleteService(@PathVariable Integer id) {
        serviceService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/service")
    public ResponseEntity<String> editService(@RequestBody Map<String, Object> service){
        boolean isApp = (boolean) service.get("checkbox");
        if (isApp) {
            appService.update(service);
        } else {
            serviceService.update(service);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
