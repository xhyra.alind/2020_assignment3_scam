package it.rage_against_the_svm.scam.controllers;

import it.rage_against_the_svm.scam.models.Topic;
import it.rage_against_the_svm.scam.services.TopicService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TopicController {
    private final TopicService topicService;
    private HashMap<Integer, ArrayList<Integer>> parents;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping("/topic")
    public String index(Model m,
                        @RequestParam(name="name",
                                      required = false,
                                      defaultValue = "") String name,
                        @RequestParam(name="description",
                                      required = false,
                                      defaultValue = "") String description,
                        @RequestParam(name="topics",
                                      required = false) List<String> topicParents) {

        List<Integer> parentList = new ArrayList<>();

        if (topicParents != null) {
            topicParents.forEach(a -> parentList.add(Integer.parseInt(a)));
        }

        ArrayList<Topic> topics = (ArrayList<Topic>) topicService.getTopics(name, description, parentList);
        HashMap<Integer, String> names = (HashMap<Integer, String>) topicService.getNames();
        m.addAttribute("names", names);
        m.addAttribute("topics", topics);

        parents = topicService.getParents();

        Map<Integer, String> p = new HashMap<>();
        topics.forEach(t -> {
            List<String> ps = new ArrayList<>();
            parents.get(t.getId())
                    .forEach(i -> ps.add(topicService.isPresent(i) ? topicService.getOne(i).getName() : ""));
            p.put(t.getId(), ps.toString()
                                .replace('[', '\0')
                                .replace(']', '\0'));
        } );

        m.addAttribute("parents", p);

        return "topic";
    }

    @PostMapping("/topic")
    public ResponseEntity<String> addTopic(@RequestBody Map<String, Object> topic) {
        topicService.create(topic);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/topic/graph")
    @ResponseBody
    public String getGraph() {
        return topicService.getGraph();
    }

    @DeleteMapping("/topic/{id}")
    public ResponseEntity<String> deleteTopic(@PathVariable Integer id) {

        topicService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @PutMapping("/topic")
    public ResponseEntity<String> editTopic(@RequestBody Map<String, Object> topic) {

        topicService.update(topic);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
