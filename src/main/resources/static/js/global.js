function delFnc(controller, id){
    $.ajax({
        type:'DELETE',
        url:'/'+controller+'/'+id,
        success:function(){location.reload();}
    });
}
function editFnc(controller){
    let data = "";
    switch(controller){
        case "admin":
            data = {
                id: $("#editId").val(),
                name: $("#editName").val(),
                mail: $("#editEmail").val(),
                vatin: $("#editVatin").val()
            };
            break;
        case "service":
            data = {
                id: $("#editId").val(),
                name: $("#editName").val(),
                link: $("#editLink").val(),
                userAvg: $("#editUserAvg").val(),
                hosting : $("#editHosting").val(),
                admin: $("#editAdminSelect").val(),
                topics: $("#editTopicsSelect").val(),
                checkbox: $("#editType").text() == "App"
            };
            if(data["checkbox"]){
                data["numberDevice"] = $("#editDevicesNumber").val(); //numberDevice
                data["compatibility"] = $("#editCompatibility").val(); //Compatibility
            }

            break;
        case "topic":
            data = {
                id: $("#editId").val(),
                name: $("#editName").val(),
                description: $("#editDesc").val(),
                parents:$("#editTopicsSelect").val()
            };
            break;
        default:break;
    }
    $.ajax({
        type:'PUT',
        url:'/'+controller,
        contentType: 'application/json',
        data: JSON.stringify(data),
        success:function(){location.reload();}
    });
}
function cancelEditFnc(controller,id){
    switch(controller){
        case "admin":
            $(".editDiv").remove();
            $(".selectedEdit").removeClass("selectedEdit");
            break;
        case "service":
            $(".editDiv").remove();
            break;
        case "topic":
            $(".selectedEdit").removeClass("selectedEdit");
            $("#itemsEditTable").hide();
            $('#editRow').remove();
            $('#itemRow').remove();
            $("#topicTableHeader").show();
            break;
        default:break;
    }
}
function subFnc(controller){
    let data = "";
    switch(controller){
        case "admin":
            data = {
                name:$("#name").val(),
                vatin:$("#vatin").val(),
                mail:$("#mail").val()
            }
            break;
        case "service":
            data = {
                name:$("#name").val(),
                link:$("#link").val(),
                userAvg:$("#userAvg").val(),
                hosting:$("#hosting").val(),
                admin:$("#admin").val(),
                topics:$("#topics").val()
            };
            if($("#checkbox").prop("checked")){
                data["numberDevice"] = $("#numberDevice").val();
                data["compatibility"] = $("#compatibility").val();
            }
            data["checkbox"] = $("#checkbox").prop("checked"); //To decide if create Service or App

            break;
        case "topic":
            data = {
                name:$("#name").val(),
                description:$("#description").val(),
                parents:$('#nameOpt').val()
            }
            break;
        default:break;
    }
    $.ajax({
        type:'POST',
        url:'/'+controller,
        contentType:'application/json',
        data:JSON.stringify(data),
        success: function(){location.reload();}
    });
}
function toggleEdit(controller,id){
    let oldName = "";
    let tr = "";
    switch(controller){
        case "admin":
            $(".editDiv").remove();
            oldName = $("#idCell"+id).next().text();
            let oldVatin = $("#idCell"+id).next().next().text();
            let oldEmail = $("#idCell"+id).next().next().next().text();
            let toInsert = "<tr class='editDiv'>" +
                "<td><input type='hidden' id='editId' value='"+id+"'></td>" +
                "<td><input type='text' placeholder='Name' id='editName' value='"+oldName+"'></td>" +
                "<td><input type='text' placeholder='VAT number' id='editVatin' value='"+oldVatin+"'></td>" +
                "<td><input type='text' placeholder='Email' id='editEmail' value='"+oldEmail+"'></td>" +
                "<td><a class='btn btn-success' style='color:white' onclick='editFnc(\"admin\")'>Confirm</a></td>" +
                "<td><a class='btn btn-danger' style='color:white' onclick='cancelEditFnc(\"admin\",-1)'>Cancel</a></td>" +
                "<td></td>" +
                "</tr>";
            tr = $("#idCell"+id).parent();
            $(toInsert).insertAfter($(tr));
            $("#idCell"+id).parent().addClass("selectedEdit");
            break;
        case "service":
            let editAdmin = $("#admin").clone()[0];
            editAdmin.id="editAdminSelect";

            let editTopics = $("#topics").clone()[0];
            editTopics.id="editTopicsSelect";

            $(".editDiv").remove();
            oldName = $("#idCell"+id).next().text();
            let oldLink = $("#idCell"+id).next().next().text();
            let oldUserAvg = $("#idCell"+id).next().next().next().text();
            let oldHosting= $("#idCell"+id).next().next().next().next().text();
            let oldType = $("#idCell"+id).next().next().next().next().next().next().next().text();
            let oldCompatibility = $("#idCell"+id).next().next().next().next().next().next().next().next().text();
            let oldDevicesNumber = $("#idCell"+id).next().next().next().next().next().next().next().next().next().text();
            let toinsert = "<tr class='editDiv' id='editRow'>" +
                            "<td><input type='hidden' id='editId' value='"+id+"'></td>" +
                            "<td><input type='text' placeholder='Name' id='editName' value='"+oldName+"'></td>" +
                            "<td><input type='text' placeholder='Link' id='editLink' value='"+oldLink+"'></td>" +
                            "<td><input type='number' placeholder='User average' id='editUserAvg' value='"+oldUserAvg+"'></td>" +
                            "<td><input type='text' placeholder='Hosting' id='editHosting' value='"+oldHosting+"'></td>" +
                            "<td id='editAdmin'></td>" +
                            "<td id='editTopics'></td>" +
                            "<td id='editType'>"+oldType+"</td>"+
                            "<td><input type='text' placeholder='Compatibility' id='editCompatibility' value='"+oldCompatibility +"'></td>" +
                            "<td><input type='number' placeholder='DevicesNumber' id='editDevicesNumber' value='"+oldDevicesNumber +"'></td>" +
                            "<td><a class='btn btn-success' style='color:white' onclick='editFnc(\"service\")'>Confirm</a></td>" +
                            "<td><a class='btn btn-danger' style='color:white' onclick='cancelEditFnc(\"service\",-1)'>Cancel</a></td>" +
                            "<td></td>" +
                            "</tr>";
            tr = $("#idCell"+id).parent();
            $(toinsert).insertAfter($(tr));
            $("#editAdmin").append(editAdmin);
            $("#editTopics").append(editTopics);
            $("#editTopicsSelect").bsMultiSelect();
            if (oldType=="Service"){
                $("#editCompatibility").remove();
                $("#editDevicesNumber").remove();
            }
            break;
        case "topic":
            let editTopic = $("#nameOpt").clone()[0];
            editTopic.id="editTopicsSelect";
            let editRow =
                "<tr id='editRow'>" +
                "<td class='idTh'><input type='hidden' id='editId' value='"+id+"'></td>" +
                "<td class='nameTh'><input type='text' id='editName' value='"+$("#nodeName").text()+"'></td>" +
                "<td class='descTh'><input type='text' id='editDesc' value='"+$("#nodeDesc").text()+"'></td>" +
                "<td id='editTopics' class='parentsTh'></td>" +
                "<td class='btnTh'><a class='btn btn-success' style='color:white' onclick='editFnc(\"topic\")'>Confirm</a></td>" +
                "<td class='btnTh'><a class='btn btn-danger' style='color:white' onclick='cancelEditFnc(\"topic\",-1)'>Cancel</a></td>" +
                "</tr>";
            $('#editRow').remove();
            $("#itemsEditTable").show();
            $("#itemsEditTable").append($(editRow));
            $("#editTopics").append(editTopic);
            $("#editTopicsSelect option[value="+id+"]").remove();
            $("#editTopicsSelect").bsMultiSelect();
            break;
        default:break;
    }
}